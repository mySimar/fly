var gulp = require('gulp'),
	less = require('gulp-less'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglifyjs'),
	autoprefixer = require('gulp-autoprefixer');

gulp.task('less', function() {
	return gulp.src('app/less/**/*.less')
		.pipe(less())
		.pipe(autoprefixer(['last 3 versions'], { cascade: true }))
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('sass', function() {
	// return gulp.src(['app/sass/*.sass', 'app/sass/*.scss'])
	return gulp.src('app/sass/**/*.+(scss|sass)')
		.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
		.pipe(autoprefixer(['last 3 versions'], { cascade: true }))
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js'
	])
		.pipe(concat('libs.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('app/js'));
});

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false
	});
});

gulp.task('watch', ['browser-sync', 'less', 'sass', 'scripts'], function() {
	gulp.watch('app/less/*.less', ['less']);
	// gulp.watch(['app/sass/*.sass', 'app/sass/*.scss'], ['sass']);
	gulp.watch(['app/sass/*.+(scss|sass)'], ['sass']);
	gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/js/**/*.js', browserSync.reload);
});
